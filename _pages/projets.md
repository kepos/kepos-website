---
layout: page
title-first: La Serre
title-last: à projets
permalink: /serre-a-projets
category: projets
order: 2
short_description: Kèpos accompagne l’émergence de projets de création d’activités en lien avec la transition écologique sur son territoire.
in_homepage: true
---

La Serre à projets est un dispositif partenarial qui accompagne l’émergence de projets de création d’activités en lien avec la transition écologique sur le territoire nancéien. Il sera opérationnel à partir du mois de septembre 2019.

*- A venir -*