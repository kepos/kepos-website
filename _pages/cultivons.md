---
layout: page
title-first: Cultivons
title-last: ensemble !
permalink: "/cultivons-ensemble"
category: cultivons
order: 4
short_description: Kèpos dispose d’une expertise des questions de transition, qu’elle
  propose aux entreprises et territoires, en Lorraine et ailleurs.
in_homepage: true

---
La transition écologique est une question clé pour la pérennité des entreprises et l'avenir des territoires. A travers Kèpos, notre ambition est de proposer à chacun des méthodes et outils pour enclencher son propre processus de transformation.