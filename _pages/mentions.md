---
layout: page
title-first: Mentions
title-last: légales
permalink: "/mentions-legales"
category: mentions
short_description: 

---
## Editeur

Association Kèpos  
51, rue de la République  
54140 Jarville-la-Malgrange

Directeur de la publication : Emmanuel Paul, Président

## Hébergement

Site hébergé chez Netlify

Netlify  
2325 3rd St #215  
San Francisco  
CA 94107  
USA

## Crédits

* Développement du site : [L'assembleuse](https://www.lassembleuse.fr/)
* Icônes : [Fontawesome](https://fontawesome.com/)
* Illustrations : [Caroline Antoine](https://www.caroline-antoine.com/ "Caroline Antoine"), tous droits réservés
* Logo : [lagence235°](http://www.lagence235.com/ "lagence235°")

## Réutilisation des contenus

Sauf mention contraire, les contenus de ce site sont sous licence [Creative Commons BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

## Comment créditer les contenus de ce site

L’utilisateur qui souhaite réutiliser les contenus de ce site doit impérativement indiquer :

1. Le titre associé au contenu,
2. La paternité du contenu par la mention suivante : « Source : Kèpos– www.kepos.fr »,
3. Le suivi de la date à laquelle le contenu a été extrait du site : « JJ/MM/AAAA »,
4. L’indication de la licence sous laquelle le contenu a été mis à disposition