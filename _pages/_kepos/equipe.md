---
title-first: Notre
title-last: équipe
order: 3
short_description: Notre équipe réunit des professionnels engagés et conscients des
  enjeux environnementaux en cours.

---
Kèpos est porté par une équipe engagée et ambitieuse, profondément marquée par les enjeux écologiques, qui cherche à proposer des chemins de transformation à son territoire et ses acteurs.

## Emmanuel Paul

Initiateur et coordonnateur de Kèpos

_"Professionnel confirmé du développement territorial et de l’accompagnement de projets entrepreneuriaux, je porte aujourd’hui la dynamique Kèpos, autour de la transition écologique des entreprises et des territoires. En tant que père de famille, professionnel engagé et citoyen attentif, je suis interpellé par la puissance des enjeux écologiques auxquels l’humanité se trouve confrontée. Ceux-ci, par leur aspect systémique, dessinent un horizon qui doit radicalement questionner nos projets et nos actions. C’est la position que je  défends personnellement et professionnellement en posant la question de leur propre transition écologique à toute une série d'acteurs (porteurs de projets, entreprises, territoires ou institutions), afin de convertir hommes et organisations à la sobriété et à la responsabilité."_

## Laure Hammerer

Consultante en transition écologique

_"C'est avec conviction et enthousiasme que je m'engage aujourd'hui dans l'accompagnement à la transition écologique. Les défis environnementaux auxquels nous faisons face doivent être adressés par des personnes lucides, engagées et dynamiques. J'ai à coeur de convaincre l'ensemble des acteurs de notre société qu'il est nécessaire dès maintenant de faire grandir leurs organisations vers une croissance plus responsable et solidaire. Avec Kèpos, je vous propose de mettre mon expérience d'ingénieur, de manager et d'entrepreneuse à votre service pour réfléchir et agir à vos côtés dans votre ambitieuse démarche de transformation"_