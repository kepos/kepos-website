---
layout: page
title-first: Nous
title-last: contacter
permalink: /contact
category: contact
short_description: 
---

Kèpos est une association de préfiguration d’une SCIC, dont la transformation en SCIC est prévue pour le mois de juin 2019. Son siège est à Jarville-la-Malgrange, dans la métropole de Nancy.

{% include contact-block.html %}


### Blog

Kèpos anime un blog autour des questions de transition écologique, disponible à l’adresse suivante : [transition-ecologique.org](https://transition-ecologique.org/)