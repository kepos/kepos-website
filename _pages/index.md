---
layout: home
title: Accueil
title-first: Kèpos
title-last: en quelques mots
permalink: "/"

---
La SCIC Kèpos réunit un ensemble de Très Petites Entreprises qui se sont données pour mission d’oeuvrer à la transition écologique du territoire. Par l’accompagnement, la coopération et la mutualisation, elles se structurent pour passer des seuils. Ensemble, elles proposent leurs services à tout acteur public ou privé qui voudrait enclencher sa propre transition écologique. Elles contribuent à l'émergence de nouvelles activités de transition sur le territoire.