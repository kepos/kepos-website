---
layout: page
title-first: Nos
title-last: partenaires
permalink: "/partenaires"
category: partenaires
in_homepage: false
---
Kèpos est soutenu par un ensemble de partenaires engagés, qui apportent un appui technique ou financier à sa mise en œuvre.

<div class="columns is-multiline">
{% for item in site.data.partenaires %}
<div class="column is-half">
{% include partner.html partner=item %}
</div>
{% endfor %}
</div>