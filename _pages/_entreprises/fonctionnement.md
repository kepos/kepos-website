---
title-first: Notre
title-last: fonctionnement
order: 1
short_description: La SCIC Kèpos nous propose un éventail de services dans le but
  de nous faire croître de manière harmonieuse.

---
Le Jardin désigne notre communauté d'entreprises et d'associations engagées dans la transition écologique. Kèpos est pour nous un outil de développement, qui nous fait bénéficier de différents services, dans les domaines suivants : 

## Accompagnement

* Un accompagnement individuel sous la forme d’un rendez-vous mensuel qui a pour objet de relire l’actualité récente de nos structures, détecter les tendances en cours et réorienter l’action.
* Des prestations de conseil ad hoc en fonction de nos problématiques propres (management, organisation, stratégie marketing, prise en compte des questions écologiques…).
* Un service de facilitation commerciale (aide au montage de réponses à appel d’offres, mise en relation commerciale…).
* Une assistance à la recherche de financements (facilitation de réponse à appels à projets, aide au montage d’opérations de crowdfunding…).

## Coopération

* Une réunion collective par mois, où notre communauté reçoit des experts des questions écologiques, échange sur l’actualité du groupe, et apporte son soutien à chacun dans ses propres actions.
* Une assistance au montage de coopérations : facilitation des échanges de produits, de services ou d’informations entre nous, élaboration et mise sur le marché d’une offre commune en matière de transition écologique des organisations, réponse conjointe à des appels d’offres…

## Mutualisation

* Une communication partagée visant à mettre en récit collectivement la transition écologique sur le territoire : projet d’une exposition d’art contemporain sur la transition écologique, animation d’événements pour professionnels ou particuliers, animation du blog [www.transition-ecologique.org](www.transition-ecologique.org).
* La mise à disposition de ressources humaines à temps partagé.
* La mise en place d’un groupement d’achats de fournitures et de services.
* La mise en route d’un pôle d’activités de la transition écologique à travers l’investissement dans un bâtiment sur le territoire nancéien.

Via cette offre de service, Kèpos propose à toute entreprise engagée un outil de développement partagé capable de lui faire passer des seuils. Son financement est assuré par nos contributions et des financements publics ou privés extérieurs. Ce jardin s’adresse aux acteurs économiques ayant passé le cap de leur création, et bénéficiant donc de la personnalité morale.