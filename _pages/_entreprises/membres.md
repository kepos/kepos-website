---
title-first: Nos
title-last: membres
order: 2
short_description: Notre jardin d’entreprises regroupe une quinzaine d’acteurs économiques
  de secteurs, métiers et marchés différents.

---
Notre collectif est formé de jeunes entreprises ou associations, créées depuis moins de cinq ans, qui sont toutes engagées dans un processus de transition, à un titre ou à un autre : stratégie marketing, process utilisés, nature des produits ou des services vendus… Nous ne sommes tous ni sur les mêmes secteurs, ni sur les mêmes marchés, ni sur les mêmes métiers. C’est ce qui nous permet d’être en synergie à la manière d’un écosystème naturel. Nous sommes majoritairement implantés autour de Nancy.

Retrouvez ci-dessous qui nous sommes et qui nous représente au sein de Kèpos :

<div class="columns is-multiline">

{% for item in site.data.membres %}
<div class="column is-half">
{% include member.html member=item %}
</div>
{% endfor %}
</div>